#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys
if sys.version_info >= (3, 0):
    import imp
    imp.reload(sys)
else:
    reload(sys)
    sys.setdefaultencoding('utf8')
sys.dont_write_bytecode = True

from pixivpy3 import *
from datetime import datetime, timedelta
import random

_REQUESTS_KWARGS = {
  # 'proxies': {
  #   'https': 'http://127.0.0.1:8888',
  # },
  # 'verify': False,       # PAPI use https, an easy way is disable requests SSL verify
}

def pixiv_recomend():
    return_list = []
    yesterday = datetime.now() - timedelta(days=1)
    aapi = AppPixivAPI(**_REQUESTS_KWARGS)
    aapi.login("venca666@gmail.com", "smookynda")   # Not required
    #json_result = aapi.illust_ranking('day', date=yesterday.strftime('%y-%m-%d'))
    json_result = aapi.illust_recommended()
    directory = "/var/www/html/pixiv"
    if not os.path.exists(directory):
        os.makedirs(directory)

    #print(type(json_result.illusts))

    # download top3 day rankings to 'dl' dir
    for illust in json_result.illusts[:3]:
        image_url = illust.meta_single_page.get('original_image_url', illust.image_urls.large)
        print("%s: %s" % (illust.title, image_url))
        # aapi.download(image_url)
        url_basename = os.path.basename(image_url)
        extension = os.path.splitext(url_basename)[1]
        name = "%d %s %s" % (illust.id, illust.title, extension)
	filename = "%d%s" % (random.randint(1, 999999999), extension)
	#add image url to list
	return_list.append(name + " https://openfun.eu/pixiv/"+filename)
	#print("https://openfun.eu/pixiv/"+filename)
        aapi.download(image_url, path=directory, name=filename)

    return return_list    

def pixiv_search(what):
    return_list = []
    yesterday = datetime.now() - timedelta(days=1)
    aapi = AppPixivAPI(**_REQUESTS_KWARGS)
    aapi.login("venca666@gmail.com", "smookynda")   # Not required
    #json_result = aapi.illust_ranking('day', date=yesterday.strftime('%y-%m-%d'))
    json_result = aapi.search_illust(what, search_target='partial_match_for_tags')
    directory = "/var/www/html/pixiv"
    if not os.path.exists(directory):
        os.makedirs(directory)

    #print(type(json_result.illusts))

    # download top3 day rankings to 'dl' dir
    for illust in json_result.illusts[:3]:
        image_url = illust.meta_single_page.get('original_image_url', illust.image_urls.large)
        print("%s: %s" % (illust.title, image_url))
        # aapi.download(image_url)
        url_basename = os.path.basename(image_url)
        extension = os.path.splitext(url_basename)[1]
        name = "%d %s %s" % (illust.id, illust.title, extension)
	filename = "%d%s" % (random.randint(1, 999999999), extension)
	#add image url to list
	return_list.append(name + " https://openfun.eu/pixiv/"+filename)
	#print("https://openfun.eu/pixiv/"+filename)
        aapi.download(image_url, path=directory, name=filename)

    return return_list    

def pixiv_get():
    return_list = []
    yesterday = datetime.now() - timedelta(days=1)
    aapi = AppPixivAPI(**_REQUESTS_KWARGS)
    aapi.login("venca666@gmail.com", "smookynda")   # Not required
    json_result = aapi.illust_ranking('day', date=yesterday.strftime('%y-%m-%d'))

    directory = "/var/www/html/pixiv"
    if not os.path.exists(directory):
        os.makedirs(directory)

    #print(type(json_result.illusts))

    # download top3 day rankings to 'dl' dir
    #for illust in json_result.illusts[:3]:
    # download random pictures
    for x in range(0, 5):
    	illust = random.choice(json_result.illusts)
        image_url = illust.meta_single_page.get('original_image_url', illust.image_urls.large)
        print("%s: %s" % (illust.title, image_url))
        # aapi.download(image_url)
        url_basename = os.path.basename(image_url)
        extension = os.path.splitext(url_basename)[1]
        name = "%d %s %s" % (illust.id, illust.title, extension)
	filename = "%d%s" % (random.randint(1, 999999999), extension)
	#add image url to list
	return_list.append(name + " https://openfun.eu/pixiv/"+filename)
	#print("https://openfun.eu/pixiv/"+filename)
        aapi.download(image_url, path=directory, name=filename)

    return return_list

if __name__ == '__main__':
    pixiv_get()
